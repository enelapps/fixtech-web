'use strict';

const Schema = use('Schema');

class CompanySchema extends Schema {
  up() {
    this.create('companies', (table) => {
      table.increments();
      table.string('name', 80).notNullable();
      table.string('logo', 80).notNullable();
      table.string('description', 250).notNullable();
      table.integer('type').notNullable();
      table.timestamps();
    });
  }

  down() {
    this.drop('companies');
  }
}

module.exports = CompanySchema;
