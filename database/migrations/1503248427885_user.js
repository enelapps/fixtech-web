'use strict';

const Schema = use('Schema');

class UserSchema extends Schema {
  up() {
    this.create('users', (table) => {
      table.increments();
      table.integer('boss').notNullable();
      table.integer('type').notNullable();
      table.integer('company_id', 80);
      table.string('name', 80).notNullable();
      table.string('surname', 80);
      table.string('birthday', 80);
      table.string('photo');
      table.string('email', 254).notNullable().unique();
      table.string('password', 60).notNullable();
      table.timestamps();
    });
  }

  down() {
    this.drop('users');
  }
}

module.exports = UserSchema;
