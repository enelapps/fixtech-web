'use strict';

/*
|--------------------------------------------------------------------------
| CompanySeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

const Company = use('App/Models/Company');

class CompanySeeder {
  async run() {
    const companies = [
      {
        name: 'Test Company',
        logo: 'test_logo.png',
        description: 'Test Company',
        type: 1,
      },
    ];

    await Company.createMany(companies);
  }
}

module.exports = CompanySeeder;
