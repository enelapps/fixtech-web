'use strict';

/*
|--------------------------------------------------------------------------
| UserSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

const User = use('App/Models/User');

class UserSeeder {
  async run() {
    const users = [
      {
        name: 'admin',
        surname: 'admin',
        email: 'admin@admin.com',
        password: '12345678',
        type: 1,
        boss: 0,
        company_id: 1,
      },
    ];

    /**
     * Charterers
     * @type {string[]}
     */
    const charterers = [
      'AkzoNobel',
      'Archer Daniels Midlands',
      'Arkema',
      'BASF',
      'Bayer',
      'Borealis',
      'BP Chemicals',
      'Braskem',
      'Bunge',
      'Celanese',
      'Chemoil',
      'Chevron Phillips Chemical',
      'DOW Chemical',
      'Evonik Industries',
      'ExxonMobil',
      'Formosa Plastics',
      'Glencore',
      'Gunvor',
      'Huntsman',
      'ICC',
      'Ineos',
      'Integra',
      'Interchem',
      'Kolmar',
      'Lanxess',
      'LlyondellBasell',
      'Louis Dreyfus',
      'Luberef',
      'LukOil',
      'Mercuria Trading',
      'Mitsubishi Chemical',
      'Mitsui Chemicals',
      'Muntajat',
      'Neste',
      'Noble Group',
      'Petrobras',
      'Petronas',
      'PPT Group',
      'Proctor & Gamble',
      'Reliance Industries',
      'Repsol',
      'SABIC',
      'SASOL',
      'Saudi Aramco',
      'Shell Chemicals',
      'Sinopec',
      'SK Oil',
      'S-Oil',
      'Solvay',
      'Sumitomo Chemical',
      'Syngenta',
      'Total',
      'Traffigura',
      'Tricon Energy',
      'Tupras',
      'Unilever',
      'Vitol',
      'Wilmar',
      'Golden Agri',
      'IOI Corporation Berhad',
    ];

    charterers.forEach((c) => {
      users.push({
        name: c,
        email: `${c.replace(/\s/g, '.').toLowerCase()}@chaterer.com`,
        password: '12345678',
        type: 4,
        boss: 0,
      });
    });

    /**
     * Brokers
     * @type {string[]}
     */
    const brokers = [
      'Braemar ACM',
      'BRS',
      'Clarksons',
      'EA Gibson',
      'Howe Robinson Partners',
      'Lorentzen & Stemoco AS',
      'Maersk Broker',
      'McQuilling',
      'Poten & Partners',
      'SPI',
      'SSY',
      'Steem 1964',
    ];

    brokers.forEach((b) => {
      users.push({
        name: b,
        email: `${b.replace(/\s/g, '.').toLowerCase()}@chaterer.com`,
        password: '12345678',
        type: 3,
        boss: 0,
      });
    });

    /**
     * Shipowners
     * @type {string[]}
     */
    const shipowners = [
      'Stolt-Nielsen',
      'Odfjell',
      'Lino',
      'Unix',
      'Ace Quantum',
      'Team Tankers',
    ];

    shipowners.forEach((s) => {
      users.push({
        name: s,
        email: `${s.replace(/\s/g, '.').toLowerCase()}@chaterer.com`,
        password: '12345678',
        type: 2,
        boss: 0,
      });
    });

    await User.createMany(users);
  }
}

module.exports = UserSeeder;
