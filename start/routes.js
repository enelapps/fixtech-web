'use strict';

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.0/routing
|
*/

const Route = use('Route');

// User Routes
Route.group(() => {
  /**
   * auth routes
   */
  Route.post('user/login', 'UserController.login')
    .validator('LoginUser');
  Route.resource('user', 'UserController')
    .middleware(new Map([
      [['index', 'show', 'store', 'update', 'destroy'], ['auth']],
    ]));

}).prefix('api')


Route.any('*', ({ view }) => view.render('main'));
