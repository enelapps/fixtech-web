export default class User {
  constructor(user) {
    if (user) {
      const {
        id,
        rut,
        name,
        surname,
        email,
        type,
        boss,
      } = user;

      this.id = id;
      this.rut = rut;
      this.name = name;
      this.surname = surname;
      this.email = email;
      this.type = type;
      this.boss = boss;
    } else {
      this.id = null;
      this.rut = null;
      this.name = null;
      this.surname = null;
      this.email = null;
      this.type = 0;
      this.boss = null;
    }
  }
}
