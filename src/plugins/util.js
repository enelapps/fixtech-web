
import auth from '../store/auth';

/**
 * Method to get js default modules
 * @param files
 * @param isArray
 * @returns {*}
 */
export function getJsModules(files, isArray = false) {
  const modules = isArray ? [] : {};
  files.keys().forEach((key) => {
    if (key === './index.js') return;
    if (isArray) files(key).default.map(r => modules.push(r));
    else modules[key.replace(/(\.\/|\.js)/g, '')] = files(key).default;
  });
  return modules;
}


/**
 * Method for protect routes
 * @param Component
 * @param rest
 * @returns {*}
 */

export function requireAuth(to, from, next) {
  if (!auth.getLocalUser()) {
    next({
      path: '/auth/login',
      query: { redirect: to.fullPath },
    });
  } else {
    next();
  }
}

export const rules = {
  empty: v => !!v || 'Campo requerido',
  number: v => (v && v.length >= 8) || 'Debe de contener 8 caracteres',
  email: v => /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/.test(v) || 'Introdusca un Email valido.',
  password: v => (v && v.length >= 8) || 'Debe de contener 8 caracteres',
};
