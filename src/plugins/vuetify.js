import Vue from 'vue';
import {
  Vuetify,
  VApp,
  VNavigationDrawer,
  VList,
  VBtn,
  VIcon,
  VToolbar,
  transitions,
  VGrid,
  VMenu,
  VCard,
  VBadge,
  VDivider,
  VDialog,
  VBottomNav,
  VTextField,
  VDataTable,
  VTooltip,
  VCheckbox,
  VForm,
  VSelect,
  VProgressLinear,
  VTabs,
  VSnackbar,
} from 'vuetify';
import 'vuetify/dist/vuetify.css';

Vue.use(Vuetify, {
  components: {
    VApp,
    VNavigationDrawer,
    VList,
    VBtn,
    VIcon,
    VGrid,
    VToolbar,
    transitions,
    VMenu,
    VCard,
    VBadge,
    VDivider,
    VDialog,
    VBottomNav,
    VTextField,
    VDataTable,
    VTooltip,
    VCheckbox,
    VForm,
    VSelect,
    VProgressLinear,
    VTabs,
    VSnackbar,
  },
  theme: {
    primary: '#132c48',
    secondary: '#424242',
    accent: '#82B1FF',
    error: '#FF5252',
    info: '#2196F3',
    success: '#4CAF50',
    warning: '#FFC107',
  },
});

/*
Vue.use(Vuetify, {
  theme: {
    primary: '#0c86e2',
    secondary: '#00aad2',
    accent: '#82B1FF',
    error: '#FF5252',
    info: '#2196F3',
    success: '#4CAF50',
    warning: '#FFC107',
  },
});
*/
