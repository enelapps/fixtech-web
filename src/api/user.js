import axios from 'axios';
import { apiUrl } from './';

export default {
  async login(params) {
    return axios.post(`${apiUrl}/user/login`, params);
  },
  async fetch(params) {
    return axios.get(`${apiUrl}/user`, { params });
  },
  async create(params) {
    return axios.post(`${apiUrl}/user`, params);
  },
  async update(params) {
    return axios.put(`${apiUrl}/user/${params.id}`, params);
  },
  async delete(id) {
    return axios.delete(`${apiUrl}/user/${id}`);
  },
};
