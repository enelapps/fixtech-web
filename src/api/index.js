// Default ApiRestClient
export const apiUrl = process.env.API_URL;

// Default Path
export const baseUrl = process.env.APP_URL;

export const xmlHttp = (type, url) => new Promise((resolve) => {
  const xmlHttpr = new XMLHttpRequest();
  xmlHttpr.open(type, url, true);
  xmlHttpr.onload = () => {
    if (xmlHttpr.readyState === 4) {
      if (xmlHttpr.status === 200) {
        resolve(xmlHttpr.responseText);
      } else {
        // console.error(xmlHttpr.statusText);
      }
    }
  };
  xmlHttpr.send(null);
});

export const xmlHttpDownloadFile = (type, url, headers, token) => new Promise((resolve) => {
  const xmlHttpr = new XMLHttpRequest();
  xmlHttpr.open(type, url, true);
  xmlHttpr.responseType = 'blob';
  if (headers) xmlHttpr.setRequestHeader('Authorization', `Bearer ${token}`);
  xmlHttpr.onreadystatechange = () => {
    if (xmlHttpr.readyState === 4) {
      resolve(xmlHttpr.response);
    }
  };
  xmlHttpr.send(null);
});

export const fullPath = process.env.FULL_PATH;
