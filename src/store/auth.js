import { EventEmitter } from 'events';
import isEmpty from 'lodash/isEmpty';

const store = new EventEmitter();
const localUser = 'user';

export default store;

store.state = {
  isAuth: false,
  isGoogleAuth: false,
  isChecked: true,
  isLogout: false,
  user: {},
};

store.getLocalUser = () => {
  const user = localStorage.getItem(localUser);
  return (user ? JSON.parse(user) : null);
};

store.actions = {
  setCurrentUser: ({ commit }, user) => {
    commit('SET_CURRENT_USER', user);
    if (user) commit('SET_AUTHORIZATION_TOKEN', user.token);
  },
  getLocalUser: ({ commit }) => {
    const user = localStorage.getItem(localUser);
    localStorage.setItem(localUser, JSON.stringify(user));
    commit('SET_CURRENT_USER', user);
    commit('SET_AUTHORIZATION_TOKEN', user.token);
    return user;
  },
  logout: ({ commit }) => new Promise((resolve) => {
    commit('LOGOUT');
    commit('SET_AUTHORIZATION_TOKEN', null);
    resolve();
  }),
};

store.mutations = {
  SET_CURRENT_USER(state, user) {
    state.user = user;
    state.isAuth = !isEmpty(user);
    state.isChecked = true;
    state.isLogout = false;
    if (user) localStorage.setItem(localUser, JSON.stringify(user));
  },
  SET_GOOGLE_AUTH(state, val) {
    state.isGoogleAuth = val;
  },
  GET_LOCAL_USER(state, user) {
    state.user = user;
    state.isAuth = !isEmpty(user);
  },
  LOGOUT: (state) => {
    state.user = {};
    state.isAuth = false;
    state.isLogout = true;
    state.isGoogleAuth = false;
    localStorage.removeItem(localUser);
  },
};
