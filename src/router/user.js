import Layout from '../layouts/Layout';
import { requireAuth } from '../plugins/util';
import Admin from '../pages/user/Admin';

export default [{
  path: '/users',
  component: Layout,
  children: [
    {
      path: 'admin',
      name: 'user-admin',
      component: Admin,
      beforeEnter: requireAuth,
    },
  ],
}];
