import Layout from '../layouts/Layout';
import { requireAuth } from '../plugins/util';

export default [{
  path: '/setting',
  component: Layout,
  children: [
    {
      path: '/',
      name: 'setting',
      component: () => import(/* webpackChunkname: "organization-chart" */'../pages/setting/Index'),
      beforeEnter: requireAuth,
    },
  ],
}];
