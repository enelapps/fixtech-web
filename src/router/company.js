import Layout from '../layouts/Layout';
import { requireAuth } from '../plugins/util';
import Admin from '../pages/company/Admin';

export default [{
  path: '/company',
  component: Layout,
  children: [
    {
      path: 'admin',
      name: 'company-admin',
      component: Admin,
      beforeEnter: requireAuth,
    },
  ],
}];
