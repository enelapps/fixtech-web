export const userTypes = [
  {
    value: 1,
    text: 'Admin',
  },
  {
    value: 2,
    text: 'Shipowners',
  },
  {
    value: 3,
    text: 'Brokers',
  },
  {
    value: 4,
    text: 'Charterer',
  },
];

export const test = [];
