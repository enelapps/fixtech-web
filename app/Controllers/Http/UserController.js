'use strict';

const User = use('App/Models/User');

class UserController {
  /**
   * List Users
   * @returns {Promise<*>}
   */
  async index({ request }) {
    const { page = 1, rowsPerPage = 10, sortBy = 'name', descending = 'desc', search = '' } = request.all();
    const users = await User.query()
      .with('company')
      .whereRaw('email like  ?', [`%${search}%`])
      .orderBy(sortBy, descending)
      .paginate(page, rowsPerPage);
    return users;
  }

  /**
   * Find User
   * @param request
   * @returns {Promise<*>}
   */
  async show({ request }) {
    const { id } = request.all();
    const users = await User.find(id);
    return users;
  }

  /**
   * Create User
   * @param request
   * @returns {Promise<*>}
   */
  async store({ request }) {
    const user = request.only(['pin', 'rut', 'name', 'lastname', 'email', 'type', 'boss']);
    user.password = user.rut;
    const result = await User.create(user);
    return result;
  }

  /**
   * Update User
   * @param request
   * @returns {Promise<void>}
   */
  async update({ request }) {
    const {
      id,
      name,
      surname,
      email,
      type,
      boss,
    } = request.all();
    const user = await User.find(id);
    user.merge({
      id,
      name,
      surname,
      email,
      type,
      boss,
    });
    await user.save();
  }

  /**
   * Delete User
   * @param request
   * @returns {Promise<void>}
   */
  async destroy({ params }) {
    const user = await User.find(params.id);
    await user.delete();
  }

  /**
   * Login
   * @param request
   * @param auth
   * @returns {Promise<*>}
   */
  async login({ request, auth }) {
    const { email, password } = request.all();
    const token = await auth
      .withRefreshToken()
      .attempt(email, password);

    return token;
  }
}

module.exports = UserController;
