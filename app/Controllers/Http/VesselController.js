'use strict';

class VesselController {
  async index() {
  }

  async create() {
  }

  async store() {
  }

  async show() {
  }

  async edit() {
  }

  async update() {
  }

  async destroy() {
  }
}

module.exports = VesselController;
